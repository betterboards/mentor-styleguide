/*------------------------------------*\
    #SPACING
\*------------------------------------*/

/*doc
---
title: Spacing
name: spacing
category: Trumps
---

Margin and padding helper classes. Use these to tweak layout on a micro level: `.(-)(m|p)(t|r|b|l|h|v)(-|+|0)`.

```
<div class="mt++">
    <!-- 96px top margin -->
</div>

<div class="-m">
    <!-- 24px negative margin -->
</div>

<div class="pv-">
    <!-- 12px top and bottom padding -->
</div>

```

## Syntax

### Type
Define either a margin or padding.

Modifier         | Class
---------------- | -------------------
Margin           | `m`
Padding          | `p`

### Direction
Specify a direction after the type; not specifying a direction applies the spacing in all directions.

Modifier         | Class
---------------- | -------------------
Top              | `t`
Right            | `r`
Bottom           | `b`
Left             | `l`
Horizontal       | `h`
Vertical         | `v`

### Amount
Specify an amount of spacing; not specifying an amount defaults to `$base-spacing-unit`.

Modifier         | Class
---------------- | -------------------
More             | `+`
Even more        | `++`
Less             | `-`
Even less        | `--`
None             | `0`

### Negative
Prefix the class to define a negative margin.

Modifier           | Class
------------------ | -------------------
Negative (margins) | `-`

*/

$margin:                            $base-spacing-unit !default;
$padding:                           $base-spacing-unit !default;

$enable-margins:                    true !default;
$enable-margins--tiny:              true !default;
$enable-margins--small:             true !default;
$enable-margins--large:             true !default;
$enable-margins--huge:              true !default;

$enable-margins--negative:          true !default;
$enable-margins--negative-tiny:     true !default;
$enable-margins--negative-small:    true !default;
$enable-margins--negative-large:    true !default;
$enable-margins--negative-huge:     true !default;

$enable-margins--none:              true !default;

$enable-paddings:                   true !default;
$enable-paddings--tiny:             true !default;
$enable-paddings--small:            true !default;
$enable-paddings--large:            true !default;
$enable-paddings--huge:             true !default;

$enable-paddings--none:             true !default;



@if ($enable-margins == true) {
    /**
     * Margin helper classes.
     *
     * Add margins.
     */
    .m       { margin:           $margin !important; }
    .mt      { margin-top:       $margin !important; }
    .mr      { margin-right:     $margin !important; }
    .mb      { margin-bottom:    $margin !important; }
    .ml      { margin-left:      $margin !important; }
    .mh      { margin-right:     $margin !important; margin-left:      $margin !important; }
    .mv      { margin-top:       $margin !important; margin-bottom:    $margin !important; }
}

@if ($enable-margins--tiny == true) {
    /**
     * Add tiny margins.
     */
    .m--     { margin:           quarter($margin) !important; }
    .mt--    { margin-top:       quarter($margin) !important; }
    .mr--    { margin-right:     quarter($margin) !important; }
    .mb--    { margin-bottom:    quarter($margin) !important; }
    .ml--    { margin-left:      quarter($margin) !important; }
    .mh--    { margin-right:     quarter($margin) !important; margin-left:     quarter($margin) !important; }
    .mv--    { margin-top:       quarter($margin) !important; margin-bottom:   quarter($margin) !important; }
}

@if ($enable-margins--small == true) {
    /**
     * Add small margins.
     */
    .m-      { margin:           halve($margin) !important; }
    .mt-     { margin-top:       halve($margin) !important; }
    .mr-     { margin-right:     halve($margin) !important; }
    .mb-     { margin-bottom:    halve($margin) !important; }
    .ml-     { margin-left:      halve($margin) !important; }
    .mh-     { margin-right:     halve($margin) !important; margin-left:   halve($margin) !important; }
    .mv-     { margin-top:       halve($margin) !important; margin-bottom: halve($margin) !important; }
}

@if ($enable-margins--large == true) {
    /**
     * Add large margins.
     */
    .m\+     { margin:           double($margin) !important; }
    .mt\+    { margin-top:       double($margin) !important; }
    .mr\+    { margin-right:     double($margin) !important; }
    .mb\+    { margin-bottom:    double($margin) !important; }
    .ml\+    { margin-left:      double($margin) !important; }
    .mh\+    { margin-right:     double($margin) !important; margin-left:      double($margin) !important; }
    .mv\+    { margin-top:       double($margin) !important; margin-bottom:    double($margin) !important; }
}

@if ($enable-margins--huge == true) {
    /**
     * Add huge margins.
     */
    .m\+\+   { margin:           quadruple($margin) !important; }
    .mt\+\+  { margin-top:       quadruple($margin) !important; }
    .mr\+\+  { margin-right:     quadruple($margin) !important; }
    .mb\+\+  { margin-bottom:    quadruple($margin) !important; }
    .ml\+\+  { margin-left:      quadruple($margin) !important; }
    .mh\+\+  { margin-right:     quadruple($margin) !important; margin-left:   quadruple($margin) !important; }
    .mv\+\+  { margin-top:       quadruple($margin) !important; margin-bottom: quadruple($margin) !important; }
}

@if ($enable-margins--none == true) {
    /**
     * Remove margins.
     */
    .m0      { margin:           0 !important; }
    .mt0     { margin-top:       0 !important; }
    .mr0     { margin-right:     0 !important; }
    .mb0     { margin-bottom:    0 !important; }
    .ml0     { margin-left:      0 !important; }
    .mh0     { margin-right:     0 !important; margin-left:      0 !important; }
    .mv0     { margin-top:       0 !important; margin-bottom:    0 !important; }
}

@if ($enable-margins--negative == true) {
    /**
     * Negative margins.
     */
    .-m      { margin:           -$margin !important; }
    .-mt     { margin-top:       -$margin !important; }
    .-mr     { margin-right:     -$margin !important; }
    .-mb     { margin-bottom:    -$margin !important; }
    .-ml     { margin-left:      -$margin !important; }
    .-mh     { margin-right:     -$margin !important; margin-left:      -$margin !important; }
    .-mv     { margin-top:       -$margin !important; margin-bottom:    -$margin !important; }

}

@if ($enable-margins--negative-tiny == true) {
    /**
     * Tiny negative margins.
     */
    .-m--    { margin:           quarter(-$margin) !important; }
    .-mt--   { margin-top:       quarter(-$margin) !important; }
    .-mr--   { margin-right:     quarter(-$margin) !important; }
    .-mb--   { margin-bottom:    quarter(-$margin) !important; }
    .-ml--   { margin-left:      quarter(-$margin) !important; }
    .-mh--   { margin-right:     quarter(-$margin) !important; margin-left:     quarter(-$margin) !important; }
    .-mv--   { margin-top:       quarter(-$margin) !important; margin-bottom:   quarter(-$margin) !important; }
}

@if ($enable-margins--negative-small == true) {
    /**
     * Small negative margins.
     */
    .-m-     { margin:           halve(-$margin) !important; }
    .-mt-    { margin-top:       halve(-$margin) !important; }
    .-mr-    { margin-right:     halve(-$margin) !important; }
    .-mb-    { margin-bottom:    halve(-$margin) !important; }
    .-ml-    { margin-left:      halve(-$margin) !important; }
    .-mh-    { margin-right:     halve(-$margin) !important; margin-left:   halve(-$margin) !important; }
    .-mv-    { margin-top:       halve(-$margin) !important; margin-bottom: halve(-$margin) !important; }
}

@if ($enable-margins--negative-large == true) {
    /**
     * Large negative margins.
     */

    .-m\+    { margin:           double(-$margin) !important; }
    .-mt\+   { margin-top:       double(-$margin) !important; }
    .-mr\+   { margin-right:     double(-$margin) !important; }
    .-mb\+   { margin-bottom:    double(-$margin) !important; }
    .-ml\+   { margin-left:      double(-$margin) !important; }
    .-mh\+   { margin-right:     double(-$margin) !important; margin-left:      double(-$margin) !important; }
    .-mv\+   { margin-top:       double(-$margin) !important; margin-bottom:    double(-$margin) !important; }
}

@if ($enable-margins--negative-huge == true) {
    /**
     * Huge negative margins.
     */
    .-m\+\+  { margin:           quadruple(-$margin) !important; }
    .-mt\+\+ { margin-top:       quadruple(-$margin) !important; }
    .-mr\+\+ { margin-right:     quadruple(-$margin) !important; }
    .-mb\+\+ { margin-bottom:    quadruple(-$margin) !important; }
    .-ml\+\+ { margin-left:      quadruple(-$margin) !important; }
    .-mh\+\+ { margin-right:     quadruple(-$margin) !important; margin-left:   quadruple(-$margin) !important; }
    .-mv\+\+ { margin-top:       quadruple(-$margin) !important; margin-bottom: quadruple(-$margin) !important; }
}

@if ($enable-paddings == true) {
    /**
     * Padding helper classes.
     *
     * Add paddings.
     */
    .p       { padding:          $padding !important; }
    .pt      { padding-top:      $padding !important; }
    .pr      { padding-right:    $padding !important; }
    .pb      { padding-bottom:   $padding !important; }
    .pl      { padding-left:     $padding !important; }
    .ph      { padding-right:    $padding !important; padding-left:    $padding !important; }
    .pv      { padding-top:      $padding !important; padding-bottom:  $padding !important; }
}

@if ($enable-paddings--tiny == true) {
    /**
     * Add tiny paddings.
     */
    .p--     { padding:           quarter($padding) !important; }
    .pt--    { padding-top:       quarter($padding) !important; }
    .pr--    { padding-right:     quarter($padding) !important; }
    .pb--    { padding-bottom:    quarter($padding) !important; }
    .pl--    { padding-left:      quarter($padding) !important; }
    .ph--    { padding-right:     quarter($padding) !important; padding-left:      quarter($padding) !important; }
    .pv--    { padding-top:       quarter($padding) !important; padding-bottom:    quarter($padding) !important; }
}

@if ($enable-paddings--small == true) {
    /**
     * Add small paddings.
     */
    .p-      { padding:           halve($padding) !important; }
    .pt-     { padding-top:       halve($padding) !important; }
    .pr-     { padding-right:     halve($padding) !important; }
    .pb-     { padding-bottom:    halve($padding) !important; }
    .pl-     { padding-left:      halve($padding) !important; }
    .ph-     { padding-right:     halve($padding) !important; padding-left:    halve($padding) !important; }
    .pv-     { padding-top:       halve($padding) !important; padding-bottom:  halve($padding) !important; }
}

@if ($enable-paddings--large == true) {
    /**
     * Add large paddings.
     */
    .p\+     { padding:           double($padding) !important; }
    .pt\+    { padding-top:       double($padding) !important; }
    .pr\+    { padding-right:     double($padding) !important; }
    .pb\+    { padding-bottom:    double($padding) !important; }
    .pl\+    { padding-left:      double($padding) !important; }
    .ph\+    { padding-right:     double($padding) !important; padding-left:   double($padding) !important; }
    .pv\+    { padding-top:       double($padding) !important; padding-bottom: double($padding) !important; }
}

@if ($enable-paddings--huge == true) {
    /**
     * Add huge paddings.
     */
    .p\+\+   { padding:           quadruple($padding) !important; }
    .pt\+\+  { padding-top:       quadruple($padding) !important; }
    .pr\+\+  { padding-right:     quadruple($padding) !important; }
    .pb\+\+  { padding-bottom:    quadruple($padding) !important; }
    .pl\+\+  { padding-left:      quadruple($padding) !important; }
    .ph\+\+  { padding-right:     quadruple($padding) !important; padding-left:    quadruple($padding) !important; }
    .pv\+\+  { padding-top:       quadruple($padding) !important; padding-bottom:  quadruple($padding) !important; }
}

@if ($enable-paddings--none == true) {
    /**
     * Remove paddings.
     */
    .p0      { padding:          0 !important; }
    .pt0     { padding-top:      0 !important; }
    .pr0     { padding-right:    0 !important; }
    .pb0     { padding-bottom:   0 !important; }
    .pl0     { padding-left:     0 !important; }
    .ph0     { padding-right:    0 !important; padding-left:     0 !important; }
    .pv0     { padding-top:      0 !important; padding-bottom:   0 !important; }
}