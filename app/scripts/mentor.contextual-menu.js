/*------------------------------------*\
  #CONTEXTUAL-MENU
\*------------------------------------*/

(function($) {
  'use strict';

  Mentor.libs.contextualMenu = {
    name: 'contextualMenu',

    settings: {
      targetClass: '.contextual-menu',
      menuClass: '.contextual-menu__content'
    },

    init: function() {
      var self = this;

      $(self.settings.targetClass).click(function(event) {
        var menu = $(this).find(self.settings.menuClass);

        menu.toggle();

        event.stopPropagation();

        $(document).click(function(){
          menu.hide();
        });
      });
    }
  };

  Mentor.libs.contextualMenu.init();
}(jQuery));
