/*------------------------------------*\
  #UPLOADS
\*------------------------------------*/

(function($) {
  'use strict';

  Mentor.libs.uploads = {
    name: 'uploads',

    settings: {
      dropzoneClass: '.upload',

      dropzoneSettings: {
        url: 'https://boardwise.s3.amazonaws.com',
        clickable: '.upload__clickable',
        previewsContainer: '.upload__files',
        maxFiles: 1,
        thumbnailHeight: 80,
        thumbnailWidth: 80,

        previewTemplate:
          '<li class="slat">
            <div class="media">
              <div class="media__img">
                <img data-dz-thumbnail width="80" height="80" />
              </div>

              <div class="media__body">
                <p class="delta mb">
                  <a data-dz-remove class="alpha float--right plain -mt- dark-ui-color" href="">&times;</a>
                  <span data-dz-name></span>&nbsp;
                  <small class="dark-ui-color" data-dz-size></small>
                </p>

                <p class="-mt- mb-- zeta" data-dz-errormessage></p>

                <div class="progress progress--small">
                  <span class="brand-round progress__filling" style="width:0%;" data-dz-uploadprogress></span>
                </div>
              </div>
            </div>
          </li>',

        accept: function(file, done) {
          file.postData = [];

          $.ajax({
            url: '/admin/prepare-upload',
            data: { name: file.name, type: file.type, size: file.size },
            type: 'POST',

            success: function(response) {
              file.custom_status = 'ready';
              file.postData = response;

              self.isUploading = false;

              var upload = {
                url: 'https://boardwise.s3.amazonaws.com/' + response.key,
                name: file.name,
                size: file.size
              }

              $('.upload__current').val(JSON.stringify(upload));

              done();
            },

            error: function(response) {
              file.custom_status = 'rejected';

              if (response.responseText) {
                response = parseJsonMsg(response.responseText);
              }

              if (response.message) {
                done(response.message);
              } else {
                done('Error preparing the upload.');
              }
            }
          });
        },

        sending: function(file, xhr, formData) {
          $.each(file.postData, function(k, v){
            formData.append(k, v);
          });

          Mentor.libs.uploads.isUploading = true;
        },

        complete: function() {
          Mentor.libs.uploads.isUploading = false;
        },

        init: function() {
          var currentFile = $('.upload__current').val()

          if (currentFile) {
            var data = JSON.parse(currentFile);

            data.accepted = true;
            $('.upload').addClass('dz-max-files-reached')

            this.emit('addedfile', data);
            this.emit('thumbnail', data, data.url);
            this.files.push(data);
          }

          console.log(this.getAcceptedFiles().length)
        }
      }
    },

    isUploading: false,

    dropzone: function() {
      var self = this;

      $(document).ready(function(){
        new Dropzone(
          document.querySelector(self.settings.dropzoneClass),
          self.settings.dropzoneSettings
        );
      })

      window.onbeforeunload = function() {
        if (self.isUploading) {
          return 'Hold up, your files are still uploading.';
        }
      };
    },

    init: function() {
      this.dropzone();
    }
  };

  Mentor.libs.uploads.init();
}(jQuery));