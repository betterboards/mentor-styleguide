/*------------------------------------*\
	#ALERT
\*------------------------------------*/

(function($) {
	'use strict';

	Mentor.libs.alert = {
		name: 'alert',

		settings: {
			alertClass: '.alert',
			alertCloseClass: '.alert__close'
		},

		init: function() {
			var scope = this;

			$(document).on('click', scope.settings.alertCloseClass, function(ev) {
				ev.preventDefault();

				scope.dismissAlert($(this));
			});
		},

		dismissAlert: function(trigger) {
			var targetAlert = trigger.closest(this.settings.alertClass);
			targetAlert.remove();
		}
	};

	Mentor.libs.alert.init();
}(jQuery));