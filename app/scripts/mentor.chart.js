/*------------------------------------*\
	MENTOR
\*------------------------------------*/

Chart.defaults.global = {
	// Boolean - Whether to animate the chart
	animation: false,

	// Number - Number of animation steps
	animationSteps: 60,

	// String - Animation easing effect
	animationEasing: 'easeInOut',

	// Boolean - If we should show the scale at all
	showScale: true,

	// Boolean - If we want to override with a hard coded scale
	scaleOverride: false,

	/**
	 * Required if scaleOverride is true
	 */

		// Number - The number of steps in a hard coded scale
		scaleSteps: null,

		// Number - The value jump in the hard coded scale
		scaleStepWidth: null,

		// Number - The scale starting value
		scaleStartValue: null,

	// String - Colour of the scale line
	scaleLineColor: Mentor.settings.lightUiColor,

	// Number - Pixel width of the scale line
	scaleLineWidth: 1,

	// Boolean - Whether to show labels on the scale
	scaleShowLabels: true,

	// Interpolated JS string - can access value
	scaleLabel: '<%=value%>',

	// Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
	scaleIntegersOnly: true,

	// Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	scaleBeginAtZero: false,

	// String - Scale label font declaration for the scale label
	scaleFontFamily: Mentor.settings.baseFontFamily,

	// Number - Scale label font size in pixels
	scaleFontSize: Mentor.settings.baseFontSize - 2,

	// String - Scale label font weight style
	scaleFontStyle: 'normal',

	// String - Scale label font colour
	scaleFontColor: Mentor.settings.darkUiColor,

	// Boolean - whether or not the chart should be responsive and resize when the browser does.
	responsive: true,

	// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	maintainAspectRatio: true,

	// Boolean - Determines whether to draw tooltips on the canvas or not
	showTooltips: true,

	// Array - Array of string names to attach tooltip events
	tooltipEvents: ['mousemove', 'touchstart', 'touchmove'],

	// String - Tooltip background colour
	tooltipFillColor: Mentor.settings.baseTextColor,

	// String - Tooltip label font declaration for the scale label
	tooltipFontFamily: Mentor.settings.baseFontFamily,

	// Number - Tooltip label font size in pixels
	tooltipFontSize: Mentor.settings.baseFontSize - 2,

	// String - Tooltip font weight style
	tooltipFontStyle: 'normal',

	// String - Tooltip label font colour
	tooltipFontColor: Mentor.settings.baseBackgroundColor,

	// String - Tooltip title font declaration for the scale label
	tooltipTitleFontFamily: Mentor.settings.baseFontFamily,

	// Number - Tooltip title font size in pixels
	tooltipTitleFontSize: Mentor.settings.baseFontSize,

	// String - Tooltip title font weight style
	tooltipTitleFontStyle: 'normal',

	// String - Tooltip title font colour
	tooltipTitleFontColor: Mentor.settings.baseBackgroundColor,

	// Number - pixel width of padding around tooltip text
	tooltipYPadding: parseInt(Mentor.settings.baseSpacingUnit / 4),

	// Number - pixel width of padding around tooltip text
	tooltipXPadding: parseInt(Mentor.settings.baseSpacingUnit / 4),

	// Number - Size of the caret on the tooltip
	tooltipCaretSize: parseInt(Mentor.settings.baseSpacingUnit / 3),

	// Number - Pixel radius of the tooltip border
	tooltipCornerRadius: parseInt(Mentor.settings.baseSpacingUnit / 4),

	// Number - Pixel offset from point x to tooltip edge
	tooltipXOffset: parseInt(Mentor.settings.baseSpacingUnit / 2),

	// String - Template string for single tooltips
	tooltipTemplate: '<%if (label){%><%=label%>: <%}%><%= value %>',

	// String - Template string for single tooltips
	multiTooltipTemplate: '<%= value %>',

	// Function - Will fire on animation progression.
	onAnimationProgress: function(){},

	// Function - Will fire on animation completion.
	onAnimationComplete: function(){}
};