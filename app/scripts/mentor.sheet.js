/*------------------------------------*\
	#SHEET
\*------------------------------------*/

(function($) {
	'use strict';

	Mentor.libs.sheet = {
		name: 'sheet',

		settings: {
			sheetClass: '.sheet',
			sheetItemClass: '.sheet__item',
			sheetSpacing: Mentor.settings.baseSpacingUnit
		},

		init: function() {
			this.calculateHeight();
		},

		calculateHeight: function() {
			var sheets = $(this.settings.sheetClass),
				sheetItems = $(this.settings.sheetItemClass),
				sheetSpacing = this.settings.sheetSpacing;

			$.each(sheets, function() {
				var sheetHeight = $(this).height();

				$.each($(this).find(sheetItems), function() {
					sheetHeight += parseInt(sheetSpacing);
				});

				$(this).height(sheetHeight);
			});
		}
	};

	Mentor.libs.sheet.init();
}(jQuery));
