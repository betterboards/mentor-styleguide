/*------------------------------------*\
	MENTOR
\*------------------------------------*/

if(typeof(Mentor) === 'undefined') {
	var Mentor = {};
};

Mentor.settings = {

	baseFontFamily:         '"proxima-nova", sans-serif',
	baseFontSize:           16,
	baseSpacingUnit:        24,

	baseBrandColor:         '#63BCCA',
	secondaryBrandColor:    '#b1e0ec',

	warningColor:           '#ffe8aa',
	errorColor:             '#f5b7ab',
	successColor:           '#c5e5de',

	baseTextColor:          '#3a4149',
	baseBackgroundColor:    '#fff',

	baseUiColor:            '#d4d3ce',
	lightUiColor:           '#f3efe6',
	darkUiColor:            '#a1a098',

	decisionsColor:         '#e45447',
	developmentColor:       '#f17842',
	dynamicsColor:          '#f2a142',
	excellenceColor:        '#e4cd2b',
	financeColor:           '#4364a6',
	fundraisingColor:       '#547353',
	governanceColor:        '#009ca6',
	innovationColor:        '#0098a5',
	investmentColor:        '#367f9f',
	leadershipColor:        '#9acd77',
	legalColor:             '#6d4f99',
	marketingColor:         '#a4469c',
	performanceColor:       '#d34887',
	relationshipsColor:     '#cb496b',
	riskColor:              '#994e5a',
	strategyColor:          '#785847',
	technologyColor:        '#4c566c'
}

Mentor.libs = {}
