/*------------------------------------*\
	#DATATABLES
\*------------------------------------*/

(function($) {
	'use strict';

	Mentor.libs.datatables = {
		name: 'datatables',

		settings: {
			dataTableClass: '.data-table',
			dataTableFilterClass: '.data-table__filter',
			dataTableSettings: {
				paging: false,
				info: false
			}
		},

		table: function() {
			return $(this.settings.dataTableClass)
				.DataTable(this.settings.dataTableSettings);
		},

		init: function() {
			var table = this.table();

			$(this.settings.dataTableFilterClass).on('keyup', function() {
				table.search(this.value).draw();
			});
		}
	};

	Mentor.libs.datatables.init();
}(jQuery));
