/*------------------------------------*\
	#REDACTOR
\*------------------------------------*/

(function($) {
	'use strict';

	Mentor.libs.redactor = {
		name: 'redactor',

		settings: {
		  redactorClass: '.redactor',
		  redactorSettings: {
		  	minHeight: 300,
				toolbarFixedBox: true,
				pastePlainText: true,
				plugins: ['video', 'table'],
				fileUpload: true,
				buttonSource: true,
				replaceDivs: false,
				buttons: [
					'html',
					'formatting',
					'bold',
					'italic',
					'unorderedlist',
					'orderedlist',
					'outdent',
					'indent',
					'image',
					'file',
					'link',
					'alignment',
					'table'
				],
				formatting: [
					'p',
					'blockquote',
					'h2',
					'h3',
					'h4'
				],
				formattingAdd: [
	        {
	          tag: 'div',
	          title: 'Two-column Text',
	          class: 'text-cols--2'
	        },
	        {
	          tag: 'div',
	          title: 'Three-column Text',
	          class: 'text-cols--3'
	        },
	        {
	          tag: 'div',
	          title: 'Blue Background',
	          class: 'secondary-brand-background p brand-round'
	        },
	        {
	          tag: 'div',
	          title: 'Green Background',
	          class: 'success-background p brand-round'
	        },
	        {
	          tag: 'div',
	          title: 'Red Background',
	          class: 'error-background p brand-round'
	        },
	        {
	          tag: 'div',
	          title: 'Yellow Background',
	          class: 'warning-background p brand-round'
	        }
        ],
				s3: '/admin/prepare-upload'
		  }
		},

		init: function() {
		  $(this.settings.redactorClass)
				.redactor(this.settings.redactorSettings);
		}
	};

	Mentor.libs.redactor.init();
}(jQuery));