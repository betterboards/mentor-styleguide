Styleguide
==========

User interface styleguide for BetterBoards Mentor.

## Contributing
* Initialize project by running `npm install` and `bower install`
* Start a server and watch the project for changes by running `gulp watch`
* Build the project for distributing by running `gulp build`

## Components

### Settings
Global variables used elsewhere in the framework

* Colors ✔
    * Brand ✔
    * Topics ✔
    * UI ✔
* Fonts ✔
* Media queries ✔
* Spacing ✔

### Tools
Mixins and functions used elsewhere in the framework.

* Functions ✔
* Mixins ✔
    * Font sizes ✔
    * Media queries ✔
    * Truncate ✔

### Generic
High-level rules including reset, normalization, and generic helpers.

* Box sizing ✔
* Clearfix ✔
* Normalize ✔
* Reset ✔
* Shared (typographic rhythm) ✔

### Base
Base styles including typography and page rules.

* Forms ✔
* Headings ✔
* Images ✔
* Lists ✔
* Page ✔
* Paragraphs ✔
* Quotes ✔
* Smallprint ✔

### Objects
Repeatable patterns that can be used to build UI components.

* Bare list ✔
* Block list ✔
* Breadcrumb ✔
* Buttons ✔
    * Pill ✔
* Flyout ✔
* Grid ✔
* Box ✔
* Media ✔
* Pagination ✔
* Progress bar ✔
* Rule ✔
* Sheets ✔
* Slat ✔
* Range
* Stat ✔
* Switch ✔
* Tabs ✔
* Tables ✔
* Textarea ✔
* Text input ✔
* UI list ✔

### Components
Collections of objects that construct a reusable part of the UI.

* Alert ✔
* Button Dropdown ✔
* Cards ✔
    * Resource
        * Library
        * Module
        * Pathway ✔
    * CPD ✔
    * Presenter ✔
    * Reputation ✔
    * Step ✔
    * User ✔
* Charts ✔
    * Bar ✔
    * Donut ✔
    * Line ✔
    * Pie ✔
    * Polar ✔
    * Radar ✔
* Questions ✔
    * Multiple Choice ✔
    * Numeric ✔
    * True/False ✔

### Trumps
Modifiers applied to objects including widths and spacing.

* Helpers ✔
    * Background ✔
    * Color ✔
    * Font ✔
    * Radius ✔
    * Shadow ✔
* Spacing ✔
* Widths ✔
