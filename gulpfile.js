'use strict';

var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    spawn = require('child_process').spawn;

gulp.task('styles', function () {
    return gulp.src('app/styles/mentor.scss')
        .pipe($.rubySass({
            style: 'expanded',
            precision: 10
        }))
        .pipe($.autoprefixer('last 1 version'))
        .pipe($.csso())
        .pipe(gulp.dest('dist/styles'))
        .pipe($.size());
});

gulp.task('scripts', function () {
    return gulp.src('app/scripts/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.uglify())
        .pipe(gulp.dest('dist/scripts'))
        .pipe($.size());
});

gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*')
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size());
});

gulp.task('images', function () {
    return gulp.src('app/images/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/images'))
        .pipe($.size());
});

gulp.task('hologram', function() {
    spawn('hologram');
});

gulp.task('clean', function () {
    // return gulp.src(['.tmp', 'dist', 'docs'], { read: false }).pipe($.clean());
});

gulp.task('build', ['styles', 'scripts', 'fonts', 'images', 'hologram']);

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

gulp.task('connect', function () {
    var connect = require('connect');

    var app = connect()
        .use(require('connect-livereload')({ port: 35729 }))
        .use(connect.static('docs'))
        .use(connect.directory('docs'));

    require('http').createServer(app)
        .listen(9000)
        .on('listening', function () {
            console.log('Started connect web server on http://localhost:9000');
        });
});

gulp.task('serve', ['connect', 'styles', 'scripts', 'fonts', 'images', 'hologram'], function () {
    require('opn')('http://localhost:9000');
});

gulp.task('watch', ['connect', 'serve'], function () {
    var server = $.livereload();

    gulp.watch([
        'docs/**/*'
    ]).on('change', function (file) {
        server.changed(file.path);
    });

    gulp.watch('app/styles/**/*', ['styles', 'hologram']);
    gulp.watch('app/scripts/**/*', ['scripts', 'hologram']);
    gulp.watch('app/fonts/**/*', ['fonts', 'hologram']);
    gulp.watch('app/images/**/*', ['images', 'hologram']);
    gulp.watch('templates/**/*', ['hologram']);
});
